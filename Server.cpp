#include "Server.h"

Server* Server::instance = nullptr;

Server* Server::GetInstance(int port)
{
	if (Server::instance == nullptr)
	{
		Server::instance = new Server(port);
	}
	return Server::instance;
}

void Server::Start()
{
	Session* session = new Session(service, lastIndex++);

	acceptor.async_accept(session->GetSocket(), boost::bind(&Server::AcceptHandler, this, session, _1));

	service.run();
}

void Server::AcceptHandler(Session * session, const boost::system::error_code err)
{
	if (err)
	{
		// Accept 종료 처리
		return;
	}

	// Accept 성공
	int index = session->GetIndex();
	sessions.insert({ index, session });
	cout << "# " << index << " Session Accept" << endl;

	session->Read();

	session = new Session(service, lastIndex++);
	acceptor.async_accept(session->GetSocket(), boost::bind(&Server::AcceptHandler, this, session, _1));
}

void Server::Disconnect(int index)
{
	if (sessions.find(index) != sessions.end())
	{
		cout << "# " << index << " 연결 종료됨" << endl;
		sessions.erase(index);
	}
}

void Server::Notify(char buf[], size_t bytes)
{
	for (auto pair : sessions)
	{
		pair.second->Write(buf, bytes);
	}
}
