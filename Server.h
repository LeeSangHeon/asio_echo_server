#pragma once
#include "header.h"
#include "Session.h"


class Server
{
	io_service service;
	int port;

	tcp::endpoint endpoint;
	tcp::acceptor acceptor;

	map<int, Session*> sessions;
	int lastIndex;

	static Server* instance;

public:

	static Server* GetInstance(int port = 0);

	Server(int port) : port(port), endpoint(tcp::v4(), port), acceptor(service, endpoint){}

	void Start();

	void AcceptHandler(Session* session, const boost::system::error_code err);
	void Disconnect(int index);
	void Notify(char buf[], size_t bytes);
};

