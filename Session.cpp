#include "Session.h"
#include "Server.h"

Session::Session(io_service & service, int index) : index(index)
{
	sock = new tcp::socket(service);
}

tcp::socket & Session::GetSocket()
{
	return *sock;
}

int Session::GetIndex()
{
	return index;
}

void Session::Read()
{
	sock->async_read_some(buffer(buf), boost::bind(&Session::ReadHandler, this, sock, _1, _2));
}

void Session::Write(char data[], size_t bytes)
{
	sock->async_write_some(buffer(data, bytes), boost::bind(&Session::WriteHandler, this, sock, _1, _2));
}

void Session::ReadHandler(tcp::socket * sock, const boost::system::error_code err, size_t bytes)
{
	(sock, err, bytes);

	if (err)
	{
		Server::GetInstance()->Disconnect(index);
		return;
	}

	buf[bytes] = 0;

	cout << index << ": " << buf << endl;

	//Write(buf, bytes);
	Server::GetInstance()->Notify(buf, bytes);

	sock->async_read_some(buffer(buf), boost::bind(&Session::ReadHandler, this, sock, _1, _2));
}

void Session::WriteHandler(tcp::socket * sock, const boost::system::error_code err, size_t bytes)
{
	if (err)
	{
		// ����ó��
		return;
	}
}