#pragma once
#include "header.h"

class Server;

class Session
{
protected:
	int index;
	tcp::socket* sock;
	char buf[512];

public:
	Session(io_service& service, int index);

	tcp::socket& GetSocket();
	int GetIndex();

	void Read();
	void Write(char data[], size_t bytes);

private:
	void ReadHandler(tcp::socket* sock, const boost::system::error_code err, size_t bytes);
	void WriteHandler(tcp::socket* sock, const boost::system::error_code err, size_t bytes);
};